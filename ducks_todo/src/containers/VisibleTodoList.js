import { connect } from "react-redux";
import * as todoListModule from "../modules/todo";
import TodoList from "../components/TodoList";

// TodoListのpropsにstateを詰め込む
const mapStateToProps = (state) => {
  return {
    todos: state.Todo,
  };
};

// TodoListのpropsにtoggleTodoアクションを詰め込む
const mapDispatchToProps = (dispatch) => {
  return {
    toggleTodo: (id) => dispatch(todoListModule.toggleTodo(id)),
  };
};

// TodoListコンポーネントと連携
export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
