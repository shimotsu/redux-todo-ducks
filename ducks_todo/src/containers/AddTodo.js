// containerはcomponentsとstoreを紐付けるための橋渡し役

import { connect } from "react-redux";
// action、reducerら
import * as addTodoModule from "../modules/todo";
import Form from "../components/Form";

// mapDispatchToPropsは、addTodoアクションをStoreにDispatchしてくれる関数をpropsに詰め込む
const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: (text) => dispatch(addTodoModule.addTodo(text)),
  };
};

// Formコンポーネントと連携
export default connect(null, mapDispatchToProps)(Form);
